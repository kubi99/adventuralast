Popis programu:

Jedná se o jednoduchou hru s textovým uživatelským rozhraním. Hra probíhá v prostředí nehezkého podzemního 
města pod povrchem Země. Celé podzemní město je složené z jedenácti místností, kterými může uživatel libovolně procházet.
Cílem hry a zároveň úkolem hráče je získat jízdenku na tzv. Výtah milosti, který umožňuje návrat domů.

Autor: Jan Kubata

Verze: 1.00.0000

Spuštění: java -jar AdventuraPM.jar