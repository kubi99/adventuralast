
package cz.vse.kubata.logika;





/*******************************************************************************
 * Instance třídy PrikazProhledniBatoh implementují pro hru příkaz 
 * prohlédni batoh - obsah batohu.
 * 
 *
 * @author  Jan Kubata
 * @version 1.00.0000 — 2020-06-16
 */
public class PrikazProhledniBatoh implements IPrikaz {

    private static final String NAZEV = "prohlédni_batoh";
    private HerniPlan plan;

    /**
     * Konstruktor třídy
     * 
     * @param plan, herní plán, na kterém postava přenáší věci
     */
    public PrikazProhledniBatoh(HerniPlan plan)
    {
        this.plan = plan;
    }

    /**
     * Vykonává příkaz, který vypíše věci v batohu.
     * 
     * @return informace o věcech, které mám v batohu
     */
    @Override
    public String provedPrikaz(String... parametry) {
        Batoh batoh = plan.getBatoh();
        return  "V batohu máš tyto věci:\n"
        + batoh.getObsahBatohu();
    }

    /**
     * Metoda vrací název příkazu - slovo, které napíše hráč, aby vyvolal příkaz
     * 
     * @return název příkazu
     */
    @Override
      public String getNazev() {
        return NAZEV;
     }
}
