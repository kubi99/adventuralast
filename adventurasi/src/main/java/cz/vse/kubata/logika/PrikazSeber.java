
package cz.vse.kubata.logika;





/*******************************************************************************
 * Instance třídy PrikazSeber implementuje do hry příkaz seber. Možnost 
 * sebrání věci.
 * 
 *
 * @author  Jan Kubata
 * @version 1.00.0000 — 2020-06-16
 */
public class PrikazSeber implements IPrikaz
{
    private static final String NAZEV = "seber";
    private HerniPlan plan;
    

    /**
     * Konstruktor
     * 
     * @param plan herní plán
     */
    public PrikazSeber(HerniPlan plan )
    {
        this.plan = plan;
    }

    /**
     * Metoda pro provedení příkazu ve hře.
     * 
     * @param parametry, parametr na určení věci, kterou chcete sebrat
     * @return řetězec, který hra vypíše po vykonání příkazu
     */
    public String provedPrikaz(String... parametry){
        String odpoved = "";
        if(parametry.length == 0){
            odpoved = "Neřekl jsi, co chceš sebrat!";
        }
        else{
            String nazevVeci = parametry[0];
            Prostor aktualni = plan.getAktualniProstor();
            if(aktualni.obsahujeVec(nazevVeci)){
                Vec sebirana = aktualni.seberVec(nazevVeci);
                if(sebirana==null){
                    odpoved = "Toto nejde sebrat";
                }
                else{
                    Batoh batoh = plan.getBatoh();
                    if(batoh.vlozVec(sebirana)){
                        odpoved = "Sebral jsi tuto věc. \n\n";
                    }
                    else{
                        odpoved = "Věc nelze sebrat, neboť máš plný batoh.";
                        aktualni.vlozVec(sebirana);
                    }
                }
            }
            else{
                odpoved = "To tu není.";
            }
        }
        
        return odpoved;
    }
        
    
    /**
     * Metoda vrací název příkazu (slovo pro vyvolání příkazu).
     * 
     * @return název příkazu
     */
    public String getNazev(){
        return NAZEV;
    }
}
