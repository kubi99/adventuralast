
package cz.vse.kubata.logika;



import java.util.Map;
import java.util.HashMap;

/*******************************************************************************
 * Instance třídy Batoh představují možnost přenášet věci, které uživatel 
 * může najít ve hře.
 * 
 *
 * @author  Jan Kubata
 * @version 1.00.0000 — 2020-06-15
 */
public class Batoh
{
    private Map<String,Vec> obsahBatohu;
    private static final int Kapacita = 5;

    /**
     * Konstruktor
     */
    public Batoh()
    {
        obsahBatohu = new HashMap<>();
    }

    /**
     * Vloží věc do batohu, pokud je přenositelná.
     * 
     * @param cokoliv, kterou chceme vložit.
     * @return boolean hodnota. Když je věc v batohu tak vrací true, pokud ne, vrací false.
     */
    public boolean vlozVec(Vec cokoliv){
        if(isMisto() && cokoliv.isPrenositelna()){
            obsahBatohu.put(cokoliv.getNazev(),cokoliv);
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Getter, který vrátí informace o plnosti batohu. Zda je plný nebo stále nezaplněný.
     * 
     * @return boolean hodnota, true - je místo, false - plná kapacita
     */
    public boolean isMisto(){
        return (obsahBatohu.size() < Kapacita);
        
    }
    
    /**
     * Odebere věc z batohu podle názvu věci.
     * 
     * @param nazevVeci věci, kterou chceme odebrat z batohu.
     * @return boolean hodnota odebrání. Pokud byla věc odebraná, vrací true,
     * jinak false.
     */
    public boolean odeberVec(String nazevVeci){
        
        if(obsahujeVec(nazevVeci)){
            
            obsahBatohu.remove(nazevVeci);
            return true;
        }
        else{
            return false;
        }
    }
    
    /**
     * Tato metoda zjistí jestli batoh obsahuje danou věc.
     * 
     * @param nazevVeci hledané věci
     * @return boolean hodnota. Pokud obsahuje, tak true, jinak false.
     */
    public boolean obsahujeVec(String nazevVeci){
        for(String kod : obsahBatohu.keySet()){
            if(kod.equals(nazevVeci)){
                return true;
                
            }
        }
        return false;
    }
    
    /**
     * Vrátí věc z batohu, ale neodebere ji z něho.
     * 
     * @param nazevVeci hledané věci
     * @return hledaná věc
     */
    public Vec vratVec(String nazevVeci){
        Vec zpet = null;
        if(obsahBatohu.containsKey(nazevVeci)){
            zpet = obsahBatohu.get(nazevVeci);
        }
        return zpet;
    }
    
    /**
     * Vrací seznam věcí v batohu.
     * Využití u příkazu prohlédni batoh.
     * 
     * @return seznam věcí v batohu
     */
    public String getObsahBatohu(){
        String odpoved = " ";
        for (String zpet : obsahBatohu.keySet()){
            odpoved += " " + zpet;
        }
        return odpoved;
    }

    public Map<String, Vec> getVeciZBatohu(){
        return obsahBatohu;
    }
}

