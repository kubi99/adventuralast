
package cz.vse.kubata.logika;





/*******************************************************************************
 * Instance třídy PrikazDej implementuje pro gru příkaz dej pro výměnu 
 * věcí s postavami.
 * 
 *
 * @author  Jan Kubata
 * @version 1.00.0000 — 2020-06-15
 */
public class PrikazDej implements IPrikaz
{
    private static final String NAZEV = "dej";
    private HerniPlan plan;
    /**
     * Konstruktor třídy PrikazDej
     * 
     * @param plan
     */
    public PrikazDej(HerniPlan plan)
    {
        this.plan = plan;
    }

    
    /**
     * Metoda pro provedení příkazu ve hře.
     * Výměna věcí mezi postavami a uživatelem.
     * 
     * @param parametry, první parametr na určení obdarované postavy
     * druhý parametr na určení darované věci
     */
    public String provedPrikaz(String... parametry){
        if((parametry.length == 0)|(parametry.length == 1)){
            return "Komu a co chceš dát?";
        }
        
        
        
        String jmenoPrijemce = parametry[0];
        String nazevDarku = parametry[1];
        Batoh batoh = plan.getBatoh();
        if(batoh.obsahujeVec(nazevDarku)){
            
            if(plan.getAktualniProstor().jePostavaVProstoru(jmenoPrijemce)){
                
                Vec darek = batoh.vratVec(nazevDarku);
                Postava prijemce = plan.getAktualniProstor().vratPostavu(jmenoPrijemce);
                Pár par = prijemce.vymena(darek);
                Vec vracenaVec = par.getVracenaVec();
                
                if(vracenaVec!=null){
                    batoh.odeberVec(nazevDarku);
                    batoh.vlozVec(vracenaVec);
                    
                    
                }
                
                return par.getVracenyString();
            }
            else{
                return "Tato postava tu není.";
            }
            
        }
        else{
            return "Tuhle věc nemáš v batohu.";
        }
    }

    /**
     * Metoda vrací název příkazu - slovo pro vyvolání příkazu.
     * 
     * @return název příkazu
     */
    public String getNazev(){
        return NAZEV;
    }
}
