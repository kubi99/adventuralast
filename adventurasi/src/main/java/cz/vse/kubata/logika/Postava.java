
package cz.vse.kubata.logika;





/*******************************************************************************
 * Třída Postava představuje postavy, které se nacházejí v prostorách hry.
 * 
 *
 * @author  Jan Kubata
 * @version 1.00.0000 — 2020-06-15
 */
public class Postava
{
    private String jmeno;
    private boolean zabitelna = false;
    private boolean mrtva = false;
    private Vec chce;
    private Vec vymeni;
    private String mluvaPred;
    private String mluvaPo;
    private String mluvaNechce;
    private String mluvaChce;
    private boolean anoVymena = false;
    private Prostor zviditelnenyProstor = null;
    
    /**
     * Konstruktor pro vytvoření postavy se jménem, prvotním proslovem a
     * statusem zabitelnosti.
     * 
     * @param jmeno postavy
     * @param rec,uvodni řeč
     * @param zabitelna,boolean hodnota jestli jde postava zabít
     */
    public Postava(String jmeno, String rec, boolean zabitelna)
    {
        this.jmeno = jmeno;
        this.mluvaPred = rec;
        this.zabitelna = zabitelna;
    }

    /**
     * Setter na infomraci o smrti postavy.
     * 
     * @param mrtva hodnota, která ukazuje jestli je postava mrtvá nebo není.
     */
    public void setMrtva(boolean mrtva){
        this.mrtva = mrtva;

    }
    
    /**
     * Getter, který ukazuje jesti je postava mrtvá.
     * 
     * @return boolean hodnota, pokud je postava mrtvá - true, pokud ne - false
     */
    public boolean isMrtva(){
        return mrtva;
    }
    
    /**
     * Setter na možnost zabití postavy.
     * 
     * @param zabitelna hodnota, která ukazuje jestli je postava zabitelná nebo není.
     */
    public void setZabitelna(boolean zabitelna){
        this.zabitelna = zabitelna;
    }
    
    /**
     * Getter na zabitelnost.
     * 
     * @return boolean hodnota, pokud lze postavu zabít - true, pokud nelze - false
     */
    public boolean isZabitelna(){
        return zabitelna;
    }
    
    /**
     * Nastavuje výměnu u postav, se kterými lze měnit.
     * 
     * @param chce, věc, kterou postava chce
     * @param vymeni, věc, kterou postava má
     * @param mluvaNechce, řeč, kterou postava řekne, pokud nabízenou věc nechce
     * @param mluvaChce, řeč, kterou postava řekne, pokud nabízenou věc chce
     * @param mluvaPo, řeč, ktreou postava řekne po tom, co dostane věc, kterou chce
     */
    public void nastavVymena(Vec chce, Vec vymeni, String mluvaNechce, 
                             String mluvaChce, String mluvaPo)
    {
        // put your code here
        this.chce = chce;
        this.vymeni = vymeni;
        this.mluvaNechce = mluvaNechce;
        this.mluvaChce = mluvaChce;
        this.mluvaPo = mluvaPo;
    }
    
    /**
     * Metoda, která provede výměnu věcí s postavou.
     * 
     * @param nabidka, nabízená věc
     * @return pár - věc, kterou postava dá hráči a řeč postava s tím spojená
     */
    public Pár vymena(Vec nabidka){
        if(nabidka.equals(chce)){
            anoVymena = true;
            return new Pár(vymeni, getMluvaVymena(true));
            
        }
        else{
            return new Pár(null, getMluvaVymena(false));
            
        }
    }
    
    /**
     * Vrací řeč podle toho, zda výměna proběhla, nebo ne.
     * 
     * @return řeč, kterou postava řekne před nebo po výměně s hráčem
     */
    public String getMluva(){
        if(anoVymena){
            return mluvaPo;
        }
        else{
            return mluvaPred;
        }
    }

    /**
     * Metoda, která vrací řeč postavy při výměně podle její reakce na nabídku.
     * 
     * @param anoStaleVymena, boolean hodnota, podle toho jestli výměna proběhla nebo neproběhla
     * @return řeč postavy podle toho jestli věc chtěl nebo nechtěl
     */
    private String getMluvaVymena(boolean anoStaleVymena){
        if(anoStaleVymena){
            return mluvaChce;
        }
        else{
            return mluvaNechce;
        }
    }
    
    /**
     * Getter na jméno postavy.
     * 
     * @return jméno postavy
     */
    public String getJmeno(){
     return jmeno;   
    }
    
    /**
     * Setter na prostor, který může postava zviditelnit.
     * 
     * @param zviditelneny, prostor, který lze zviditelnit
     */
    public void setZviditelnenyProstor(Prostor zviditelneny){
        this.zviditelnenyProstor = zviditelneny;
    }
    
    /**
     * Getter na prostor, která postava může zviditelnit.
     * 
     * @return prostor, který lze zviditelnit
     */
    public Prostor getZviditelnenyProstor(){
        return zviditelnenyProstor;
    }
}
