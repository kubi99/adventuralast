package cz.vse.kubata.logika;




/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 *@version    pro školní rok 2016/2017
 */
public class HerniPlan {
    
    private Prostor aktualniProstor;
    private Prostor viteznyProstor;
    private Vec viteznaVec;
    private Batoh batoh;
    private Hra hra;
    
     /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        zalozProstoryHry();
        batoh = new Batoh();
        

    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory
        
        Prostor vytah = new Prostor("Výtah_milosti","Odlehlé zákoutí, kde se nachází výtah, kterým se dostaneš zpět na povrch Země. Avšak potřebuješ jízdenku");
        Prostor namesti = new Prostor("Netopýří_náměstí","Náměstí, které je sice rozsáhlé, ale uplně prázdné");
        Prostor ulicka = new Prostor("Zapáchající_ulička","Nebezpečná ulička plná smradu");
        Prostor obchod = new Prostor("Obchod_U_Dvou_ptakopysků","Místní smíšené zboží");
        Prostor most = new Prostor("Krokodýlí_most","Polorozpadlý kamenný most, který byl osudný pro mnoho zbloudilých duší");
        Prostor hrbitov = new Prostor("Hřbitov","Starý hřbitov, kde stojí jediný osamělý hrob");
        Prostor propast = new Prostor("Nekonečná_propast","Nekonečná propast, ze které není cesty zpět");
        Prostor zricenina = new Prostor("Zřícenina_Halmar","Nádvoří polorozpadlého hradu Halmar");
        Prostor vstupniHala = new Prostor("Vstupní_hala","Vysoká místnost, která každého zaujme strašidelnými obrazy a zchátralým schodištěm");
        Prostor pokoj = new Prostor("Pokoj_smrti","Pokoj, který hlídá nebezpečný trol");
        Prostor sklepeni = new Prostor("Sklepení","Staré sklepy, které se rozprostírající pod Halmarem");
        
        // přiřazují se průchody mezi prostory (sousedící prostory)
        ulicka.setVychod(namesti);
        namesti.setVychod(ulicka);
        namesti.setVychod(obchod);
        namesti.setVychod(most);
        namesti.setVychod(zricenina);
        obchod.setVychod(namesti);
        most.setVychod(namesti);
        zricenina.setVychod(namesti);
        vytah.setVychod(most);
        most.setVychod(vytah);
        propast.setVychod(most);
        most.setVychod(propast);
        zricenina.setVychod(hrbitov);
        hrbitov.setVychod(zricenina);
        zricenina.setVychod(vstupniHala);
        vstupniHala.setVychod(zricenina);
        vstupniHala.setVychod(sklepeni);
        vstupniHala.setVychod(pokoj);
        pokoj.setVychod(vstupniHala);
        sklepeni.setVychod(vstupniHala);
        
        hrbitov.setViditelny(false);
        propast.setSmrtelny(true);
        
        Vec pult = new Vec("pult", false);
        obchod.vlozVec(pult);
        Vec almara = new Vec("almara", false);
        zricenina.vlozVec(almara);
        Vec hrob = new Vec("hrob", false);
        hrbitov.vlozVec(hrob);
        hrob.setLzeVykopat(true);
        Vec kasna = new Vec("kašna", false);
        namesti.vlozVec(kasna);
        Vec trezor = new Vec("trezor", false);
        sklepeni.vlozVec(trezor);
        trezor.setLzeOdemknout(true);
        
        Vec dyka = new Vec("dýka", true);
        hrob.vlozVecDoVeci(dyka);
        dyka.setZbran(true);
        Vec lano = new Vec("lano", true);
        vstupniHala.vlozVec(lano);
        Vec jizdenka = new Vec("jízdenka", true);
        trezor.vlozVecDoVeci(jizdenka);
        Vec penize = new Vec("peníze", true);
        most.vlozVec(penize);
        Vec otravenyNetopyr = new Vec("otrávený_netopýr", true);
        namesti.vlozVec(otravenyNetopyr);
        
        
        
        
        
        Postava obchodnik = new Postava("obchodník","obchodník: Dobrý den, můžu nabídnout nějaký sýr?", false);
        Vec syr = new Vec("sýr", true);
        obchodnik.nastavVymena(penize, syr,"obchodník: Za tohle ti nic nedám!","obchodník: Díky moc, tady máš kus sýra.","obchodník: Budete-li mít příště znovu hlad, určitě se zastavte! Hezký den.");
        obchod.vlozPostavu(obchodnik);
        
        Postava poutnik = new Postava("poutník","poutník: Zdravím, chceš se dostat zpět do světa lidí? Zkus se podívat na hřbitov, ke kterému vede tajná cesta kolem starého dubu na nádvoří. Za kousek jídla ti rád pomůžu ještě víc.",false);
        Vec lopata = new Vec("lopata",true);
        poutnik.nastavVymena(syr, lopata, "poutník: Za tohle ti nic neřeknu!","poutník: Výborný sýr, děkuji ti. Tady máš lopatu, snad se ti bude hodit.","poutník: Doufám, že ti pomůže. Nashledanou!"); 
        poutnik.setZviditelnenyProstor(hrbitov);
        zricenina.vlozPostavu(poutnik);
        
        Postava trol = new Postava("trol", "trol: Co tady chceš, ty bídný červe???", true);
        Vec klic = new Vec("klíč", false);
        if(trol.isMrtva()){
            klic.setPrenositelna(true);
        }
        else{
            klic.setPrenositelna(false);
        }
        pokoj.vlozVec(klic);
        pokoj.vlozPostavu(trol);
        
                
        aktualniProstor = ulicka;
        viteznyProstor = vytah;
        viteznaVec = jizdenka;
    }
    
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
       aktualniProstor = prostor;
    }

    /**
     * Metoda vrací batoh, který je využit ve hře jako přenášedlo věcí.
     * 
     * @return batoh
     */
    public Batoh getBatoh(){
        return batoh;
    }
    /**
     * Tato metoda zjišťuje, zda hráč dosáhl vítězství nebo ne.
     * Když se hráč dostane do vítězné místnosti a v batohu má vítěznou věc - vrací true.
     * Když se hráč dostane do vítězné místnosti a v batohu nemá vítěznou věc - false.
     * 
     * @return boolean hodnota podle toho jestli se hráč s vítěznou věcí nachází ve v. prostoru
     */
    public boolean vitezstvi(){

        return (aktualniProstor.equals(viteznyProstor)&&batoh.obsahujeVec(viteznaVec.getNazev()));

    }
}
