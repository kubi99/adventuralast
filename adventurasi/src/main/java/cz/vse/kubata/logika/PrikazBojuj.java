
package cz.vse.kubata.logika;





/*******************************************************************************
 * Instance třídy PrikazBojuj implementuje do hry příkaz bojuj.
 * 
 *
 * @author  Jan Kubata
 * @version 1.00.0000 — 2020-06-19
 */
public class PrikazBojuj implements IPrikaz {


    private static final String NAZEV = "bojuj";
    private HerniPlan plan;
    private Hra hra;

    /**
     * Konstruktor této třídy.
     * 
     * @param plan, herní plán
     */
    public PrikazBojuj(HerniPlan plan, Hra hra)
    {
        this.plan = plan;
        this.hra = hra;
    }

    /**
     * Metoda pro provedení příkazu ve hře.
     * Příkaz "bojuj" umožňuje zabití postavi, pokud je možné ji zabít a uživatel
     * má u sebe dýku. Pozabití postavi může uživatel sebrat klíč.
     * 
     * @param parametry, jeden parametr - postava se kterou chcete bojovat
     * @return zpráva, kterou vypíše hra hráči
     */
    public String provedPrikaz(String... parametry){
        String odpoved = "";
        if(parametry.length == 0){
            odpoved = "Neřekl jsi, s kým chceš bojovat!";
        }
        else{
            String nazevTrola = parametry[0];
            
            Prostor aktualni = plan.getAktualniProstor();
            if(aktualni.jePostavaVProstoru(nazevTrola)){
                Postava trol = aktualni.vratPostavu(nazevTrola);
                
                if(trol.isZabitelna()){
                    Batoh batoh = plan.getBatoh();
                    Vec klic = aktualni.vratVec("klíč");
                    if(batoh.obsahujeVec("dýka")){
                        trol.setMrtva(true);
                        klic.setPrenositelna(true);
                        return "Zabil jsi trola a můžeš si vzít klíč k trezoru.";
                        
                    }
                    else{
                        hra.setKonecHry(true);
                        return "Jak si čekal, že dopadne souboj s trolem bez zbraně? Umřel jsi!";
                    }
                }
                else{
                    odpoved = "Tuto mírumilovnou postavu bys přece nechtěl zabít.";
                }
                
            }
            else{
                odpoved = "Zde nemáš žádného nepřítele!";
            }
            
            
        }
        
        return odpoved;
    }

    /**
     * Metoda vrací název příkazu - slovo pro vyvolání příkazu.
     * 
     * @return název příkazu
     */
    public String getNazev(){
        return NAZEV;
    }
}
