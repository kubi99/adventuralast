
package cz.vse.kubata.logika;





/*******************************************************************************
 * Instance třídy PrikazPolozVec implementuje do hry příkaz "polož", který 
 * umožňuje uvolnění místa v batohu.
 * 
 *
 * @author  Jan Kubata
 * @version 1.00.0000 — 2020-06-18
 */
public class PrikazPolozVec implements IPrikaz
{
    private static final String NAZEV = "polož";
    private HerniPlan plan;

    /**
     * Konstruktor třídy PrikazPolozVec
     * 
     * @param plan, herní plán, kde můžeme položit věc z batohu
     */
    public PrikazPolozVec(HerniPlan plan)
    {
        this.plan = plan;
    }

    /**
     *  Metoda pro provedení příkazu ve hře.
     *  
     *  @param parametry jeden parameter na určenie veci,ktorá sa pokladá
     *  
     */
    public String provedPrikaz(String... parametry){
        String odpoved = "";
        if(parametry.length == 0){
            odpoved = "Neřekl jsi, co chceš položit.";
        }
        else{
            String nazevVeci = parametry[0];
            Prostor aktualni = plan.getAktualniProstor();
            Batoh batoh = plan.getBatoh();
            if(batoh.obsahujeVec(nazevVeci)){
                Vec polozena = batoh.vratVec(nazevVeci);
                batoh.odeberVec(nazevVeci);
                aktualni.vlozVec(polozena);
                odpoved = "Položil jsi na zem věc " + nazevVeci + ".";
            }
            else{
                odpoved = "Tato věc není v batohu.";
            }
            
        }
        
        return odpoved;
    }


    /**
     *  Metoda vrací název příkazu - slovo pro vyvolání příkazu
     *  
     *  @return název příkazu
     */
    public String getNazev(){
        return NAZEV;
    }
}
