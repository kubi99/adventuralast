package cz.vse.kubata.logika;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.*;
/**
 * Trida Prostor - popisuje jednotlivé prostory (místnosti) hry
 *
 * Tato třída je součástí jednoduché textové hry.
 *
 * "Prostor" reprezentuje jedno místo (místnost, prostor, ..) ve scénáři hry.
 * Prostor může mít sousední prostory připojené přes východy. Pro každý východ
 * si prostor ukládá odkaz na sousedící prostor.
 *
 * @author Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 * @version pro školní rok 2016/2017
 */
public class Prostor {

    public String nazev;
    public String popis;
    private Set<Prostor> vychody; // obsahuje sousední místnosti
    private Map <String, Vec> veci;

    private Map <String, Postava> postavy;
    private boolean viditelny = true;
    private boolean smrtelny = false;
    /**
     * Vytvoření prostoru se zadaným popisem, např. "kuchyň", "hala", "trávník
     * před domem"
     *
     * @param nazev nazev prostoru, jednoznačný identifikátor, jedno slovo nebo
     * víceslovný název bez mezer.
     * @param popis Popis prostoru.
     */
    public Prostor(String nazev, String popis) {
        this.nazev = nazev;
        this.popis = popis;
        vychody = new HashSet<>();
        veci = new HashMap<>();

        postavy = new HashMap<>();
    }

    /**
     * Přidam seznam veci
     *
     * SeznamVeci = u mně Veci!!
     */
    public Map<String, Vec> getVeci() {
        return veci;
    }



    public Map<String, Postava> getPostavy() { return postavy; }

    /**
     * Definuje východ z prostoru (sousední/vedlejsi prostor). Vzhledem k tomu,
     * že je použit Set pro uložení východů, může být sousední prostor uveden
     * pouze jednou (tj. nelze mít dvoje dveře do stejné sousední místnosti).
     * Druhé zadání stejného prostoru tiše přepíše předchozí zadání (neobjeví se
     * žádné chybové hlášení). Lze zadat též cestu ze do sebe sama.
     *
     * @param vedlejsi prostor, který sousedi s aktualnim prostorem.
     *
     */
    public void setVychod(Prostor vedlejsi) {
        vychody.add(vedlejsi);
    }

    /**
     * Metoda equals pro porovnání dvou prostorů. Překrývá se metoda equals ze
     * třídy Object. Dva prostory jsou shodné, pokud mají stejný název. Tato
     * metoda je důležitá z hlediska správného fungování seznamu východů (Set).
     *
     * Bližší popis metody equals je u třídy Object.
     *
     * @param o object, který se má porovnávat s aktuálním
     * @return hodnotu true, pokud má zadaný prostor stejný název, jinak false
     */  
      @Override
    public boolean equals(Object o) {
        // porovnáváme zda se nejedná o dva odkazy na stejnou instanci
        if (this == o) {
            return true;
        }
        // porovnáváme jakého typu je parametr 
        if (!(o instanceof Prostor)) {
            return false;    // pokud parametr není typu Prostor, vrátíme false
        }
        // přetypujeme parametr na typ Prostor 
        Prostor druhy = (Prostor) o;

        //metoda equals třídy java.util.Objects porovná hodnoty obou názvů. 
        //Vrátí true pro stejné názvy a i v případě, že jsou oba názvy null,
        //jinak vrátí false.

       return (java.util.Objects.equals(this.nazev, druhy.nazev));       
    }

    /**
     * metoda hashCode vraci ciselny identifikator instance, ktery se pouziva
     * pro optimalizaci ukladani v dynamickych datovych strukturach. Pri
     * prekryti metody equals je potreba prekryt i metodu hashCode. Podrobny
     * popis pravidel pro vytvareni metody hashCode je u metody hashCode ve
     * tride Object
     */
    @Override
    public int hashCode() {
        int vysledek = 3;
        int hashNazvu = java.util.Objects.hashCode(this.nazev);
        vysledek = 37 * vysledek + hashNazvu;
        return vysledek;
    }
      

    /**
     * Vrací název prostoru (byl zadán při vytváření prostoru jako parametr
     * konstruktoru)
     *
     * @return název prostoru
     */
    public String getNazev() {
        return nazev;       
    }

    /**
     * Vrací "dlouhý" popis prostoru, který může vypadat následovně: Jsi v
     * mistnosti/prostoru vstupni hala budovy VSE na Jiznim meste. vychody:
     * chodba bufet ucebna
     *
     * @return Dlouhý popis prostoru
     */
    public String dlouhyPopis() {
        return "Jsi v místnosti/prostoru " + popis + ".\n\n"
                + popisVychodu()+"\n"+popisVeci() +"\n\n"+popisPostav();
    }

    /**přidal jsem
     *
     *
     */
    public String getPopis() {
        return popis;
    }

    /**
     * Vrací textový řetězec, který popisuje sousední východy, například:
     * "vychody: hala ".
     *
     * @return Popis východů - názvů sousedních prostorů
     */
    private String popisVychodu() {
        String vracenyText = "Jsou zde tyto východy:";
        for (Prostor sousedni : vychody) {
            if(sousedni.isViditelny()){
            vracenyText += " " + sousedni.getNazev() ;
        }
    }
        return vracenyText;
    }

    /**
     * Vrací prostor, který sousedí s aktuálním prostorem a jehož název je zadán
     * jako parametr. Pokud prostor s udaným jménem nesousedí s aktuálním
     * prostorem, vrací se hodnota null.
     *
     * @param nazevSouseda Jméno sousedního prostoru (východu)
     * @return Prostor, který se nachází za příslušným východem, nebo hodnota
     * null, pokud prostor zadaného jména není sousedem.
     */
    public Prostor vratSousedniProstor(String nazevSouseda) {
        List<Prostor>hledaneProstory = 
            vychody.stream()
                   .filter(sousedni -> sousedni.getNazev().equals(nazevSouseda))
                   .collect(Collectors.toList());
        if(hledaneProstory.isEmpty()){
            return null;
        }
        else {
            return hledaneProstory.get(0);
        }
    }

    /**
     * Vrací kolekci obsahující prostory, se kterými tento prostor sousedí.
     * Takto získaný seznam sousedních prostor nelze upravovat (přidávat,
     * odebírat východy) protože z hlediska správného návrhu je to plně
     * záležitostí třídy Prostor.
     *
     * @return Nemodifikovatelná kolekce prostorů (východů), se kterými tento
     * prostor sousedí.
     */
    public Collection<Prostor> getVychody() {
        return Collections.unmodifiableCollection(vychody);
    }
    
    /**
     * Metoda, která vloží věc do prostoru.
     * 
     * @param cokoliv je vkládaná věc
     */
    public void vlozVec(Vec cokoliv){
        veci.put(cokoliv.getNazev(),cokoliv);
    }
    
    /**
     * Zjišťuje, jestli daný prostor obsahuje danou věc, popřípadě daná věc obsahuje danou věc.
     * 
     * @param jmeno je název hledané věci
     * @return true-hledaná věc je v daném prostoru, false-hledaná věc není v prostoru
     */
    public boolean obsahujeVec(String jmeno){
        if(najdiVecVProstoru(jmeno) == null){ 
            for(Vec vec : veci.values()){
                if (vec.obsahujeVecTutoVec(jmeno)){  
                    return true;
                }
            }
            return false;
        }
        else{ 
            return true;
        }
    }
    
    /**
     * Vrátí údaj, zda je hledaná věc v prostoru.
     * 
     * @param jmeno, název hledané věci
     * @return hledaná věc nebo null, když v místnosti není
     */
    private Vec najdiVecVProstoru(String jmeno){ 
        Vec vec = null;
        for(Vec cokoliv : veci.values()){
            if (cokoliv.getNazev().equals(jmeno)){
                vec = cokoliv;
                break;
            }
        }
        return vec;
    }
    
    /**
     * Vrací popis věcí, které jsou v prostoru.
     * 
     * @return seznam obsahující názvy věcí v aktualním prostoru
     */
    private String popisVeci(){
        String popisVeci = "";
        if(veci.isEmpty()){
            popisVeci = "Nic tady není!";
        }
        else{
            popisVeci = "\n Jsou zde tyto věci: ";
            for (String nazev : veci.keySet()){
                popisVeci+=nazev + " ";                
            }
        }
        return popisVeci;
    }
    
    /**
     * Vrátí věc v prostoru podle jejího názvu.
     * 
     * @param nazevVeci, název hledané věci
     * @return hledaná věc, pokud je úspěšně nalezena
     */
    public Vec vratVec(String nazevVeci){
        return veci.get(nazevVeci);
    }


    
    /**
     * Metoda, která podle názvu odebere věc z prostoru.
     * 
     * @param nazev, název odebírané věci
     * @return odebraná věc
     */
    public Vec seberVec(String nazev){
        Vec sebirana = najdiVecVProstoru(nazev);
        
        if(sebirana != null && sebirana.isPrenositelna()){
            veci.remove(sebirana.getNazev(),sebirana);
            
        }
        else{
            for(Vec vec: veci.values()){
                sebirana = vec.odeberVecZVeci(nazev);
                if(sebirana != null){
                    break;
                }
            }
        }
        return sebirana;
    }
    
    /**
     * Vloží postavu do prostoru.
     * 
     * @param nekdo, vložená postava
     */
    public void vlozPostavu(Postava nekdo){
        postavy.put(nekdo.getJmeno(), nekdo);
    }
    
    /**
     * Vrací seznam (řetězec) postav v prostoru.
     * 
     * @return seznam postav
     */
    private String popisPostav(){
        String popisPostav="";
        if(postavy.isEmpty()){
            popisPostav+="Nikdo tu není.";
        }
        else{
            popisPostav +="Jsou zde tyto postavy: ";
            for (String jmeno:postavy.keySet()){
                popisPostav+= jmeno+" ";
            }
        }
        return popisPostav;
    }
    
    /**
     * Setter na viditelnost prostoru. Neviditelný prostor se neukazuje v seznamu východů.
     * 
     * @param viditelny, boolean hodnota viditelnosti
     */
    public void setViditelny(boolean viditelny){
        this.viditelny = viditelny;
    }
    
    /**
     * Vrací údaj o viditelnosti prostoru.
     * 
     * @return boolean hodnota viditelnosti
     */
    public boolean isViditelny(){
        return viditelny;
    }
    
    /**
     * Nastavuje smrtelnost prostoru. Když někdo vstoupí, hra končí.
     * 
     * @param smrtelny, boolean hodnota smrtelnosti prostoru
     */
    public void setSmrtelny(boolean smrtelny){
        this.smrtelny = smrtelny;
    }
    
    /**
     * Vrací údaj o smrtelnosti prostoru.
     * 
     * @return smrtelnost prostoru
     */
    public boolean isSmrtelny(){
        return smrtelny;
    }
    
    
    /**
     * Vrací postavu pokud je v prostoru.
     * 
     * @param nazev, název hledané věci
     * @return boolean hodnota, zda je věc v prostoru
     */
    public boolean jeVecVProstoru(String nazev){
        return veci.containsKey(nazev) ;
    }
    
    /**
     * Vrací postavu pokud je v prostoru.
     * 
     * @param jmeno, jméno hledané postavy
     * @return boolean hodnota, zda je postava v prostoru
     */
    public boolean jePostavaVProstoru(String jmeno){
        return postavy.containsKey(jmeno);
        
    }
    
    /**
     * Vrací postavu v prostoru podle jejího jména.
     * 
     * @param jmeno, jméno postavy
     * @return postava
     */
    public Postava vratPostavu(String jmeno){
        return postavy.get(jmeno);
        
    }
}
