
package cz.vse.kubata.logika;





/*******************************************************************************
 * Instance třídy PrikazOdemkni implementuje do hry příkaz odemkni.
 * Využití určité věci k odemknutí jiné věci, která obsahuje určitou věc.
 *
 * @author  Jan Kubata
 * @version 1.00.0000 — 2020-06-15
 */
public class PrikazOdemkni implements IPrikaz
{
    private static final String NAZEV = "odemkni";
    private HerniPlan plan;

    /**
     * Konstruktor
     * 
     * @param plan, herní plán (možnost odemčení věcí)
     */
       
    
    public PrikazOdemkni(HerniPlan plan)
    {
        this.plan = plan;
    }

    /**
     * Metoda pro provedení příkazu ve hře.
     * 
     * @param parametry, jeden parametr na určení věci, kterou chce uživatel odemnkout
     */
    public String provedPrikaz(String... parametry){
        String odpoved = "";
        if(parametry.length == 0){
            odpoved = "Neřekl jsi, co je potřeba odemknout!";
        }
        else{
            String nazevVeci = parametry[0];
            Prostor aktualni = plan.getAktualniProstor();
            if(aktualni.jeVecVProstoru(nazevVeci)){
                Vec trezor = aktualni.vratVec(nazevVeci);
                if(trezor.isLzeOdemknout()){
                    Batoh batoh = plan.getBatoh();
                    Vec jizdenka = trezor.vratVecVeVeci("jízdenka");
                    if(batoh.obsahujeVec("klíč")){
                        trezor.setOdemknuta(true);
                        trezor.odeberVecZVeci("jízdenka");
                        aktualni.vlozVec(jizdenka);
                        odpoved = "V trezoru leží zaprášená jízdenka.\n"+trezor.vratObsahVeci();
                        
                    }
                    else{
                        odpoved = "Trezor je zavřený, potřebuješ klíč.";
                    }
                }
                else{
                    odpoved = "Tato věc nejde odemknout.";
                }
                
            }
            else{
                odpoved = "Není tu nic zamčené.";
            }
            
            
        }
        
        return odpoved;
    }

    /**
     * Metoda vrací název příkazu - slovo pro vyvolání příkazu.
     * 
     * @return název příkazu
     */
    public String getNazev(){
        return NAZEV;
    }
}
