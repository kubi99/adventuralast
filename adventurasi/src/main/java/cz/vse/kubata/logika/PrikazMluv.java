
package cz.vse.kubata.logika;





/*******************************************************************************
 * Instance třídy PrikazMluv implementují pro hru příkaz dej - rozhovor uživatele s postavami. 
 * 
 *
 * @author  Jan Kubata
 * @version 1.00.0000 — 2020-06-21
 */
public class PrikazMluv implements IPrikaz {

    private static final String NAZEV = "mluv";
    private HerniPlan plan;

    /**
     * Konstruktor
     * 
     * @param plan, herní plán
     */
    public PrikazMluv(HerniPlan plan)
    {
        this.plan=plan;
    }

    /**
     * Metoda pro provedení příkazu ve hře.
     * 
     * @param parametry, jeden parametr, který určuje postavu, se kterou chce uživatel mluvit
     */
    public String provedPrikaz(String... parametry){
        if(parametry.length == 0){
            return "S kým bys chtěl mluvit?";
        }
        else{
            String jmeno=parametry[0];
            Prostor ted = plan.getAktualniProstor();
            if(ted.jePostavaVProstoru(jmeno)){
                Postava chtena = ted.vratPostavu(jmeno);
                if (chtena.getZviditelnenyProstor()!=null && !chtena.getZviditelnenyProstor().isViditelny()){  
                    chtena.getZviditelnenyProstor().setViditelny(true);
                    return chtena.getMluva() +"\n \nTeď už můžeš vstoupit na "+chtena.getZviditelnenyProstor().getNazev()+".";
                }
                else{
                return chtena.getMluva();
                }
           }
            else{
                return "Postava, se kterou chceš mluvit, tu není.";
            }
        }
    }

    /**
     * Metoda vrací název příkazu - slovo pro vyvolání příkazu.
     * 
     * @return název příkazu
     */
    public String getNazev(){
	    return NAZEV;
	}
}
