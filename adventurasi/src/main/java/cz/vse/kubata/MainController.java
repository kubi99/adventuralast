package cz.vse.kubata;

import cz.vse.kubata.logika.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Stop;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

/**
 * Třída MainController reprezentuje jednotlivé metody, které se provedou, vyvolají v grafické verzi hry
 */
public class MainController {


    public TextArea textOutput;
    public TextField textInput;
    private IHra hra;

    public Label locationName;
    public Label locationDescription;

    public VBox exits;
    public VBox items;
    public VBox backpack;
    public VBox persons;
    public VBox hrobs;
    public VBox trezors;
    public VBox bojuj;
    public ImageView obrazek;

    public void init(IHra hra) {
        this.hra = hra;
        update();
    }

    /**
     * Metoda aktualizuje jednotlivé metody, pozadí a nadpisy.
     */


    private void update() {

        String location = getAktualniProstor().getNazev();
        locationName.setText(location);

        String description = getAktualniProstor().getPopis();
        locationDescription.setText(description);

        InputStream stream = getClass().getClassLoader().getResourceAsStream(location + ".jpg");
        assert stream != null;
        Image img = new Image(stream);
        obrazek.setImage(img);
        obrazek.setFitHeight(1000);
        obrazek.setFitWidth(1600);


        updateExits();
        updateItems();
        updateBacpack();
        updatePersons();
        updateHrob();
        updateOdemkni();
        updateBojuj();
        kontrolaKonce();
    }

    /**
     * Metoda zjišťující aktualní prostor
     *
     * @return = aktuální prostor
     */
    private Prostor getAktualniProstor() {
    return hra.getHerniPlan().getAktualniProstor();}

    /**
     * Metoda na zobrazení věcí v prostoru a jejich sebrání
     */
    private void updateItems() {
        Collection<Vec> itemList = getAktualniProstor().getVeci().values();
        items.getChildren().clear();

        for (Vec item : itemList) {
            String itemName = item.getNazev();
            Label itemLabel = new Label(itemName);

            InputStream stream = getClass().getClassLoader().getResourceAsStream(itemName + ".jpg");
            assert stream != null;
            Image img = new Image(stream);
            ImageView imageView = new ImageView(img);
            imageView.setFitWidth(60);
            imageView.setFitHeight(40);
            itemLabel.setGraphic(imageView);

            if (item.isPrenositelna() || item.isVykopana()) {
                itemLabel.setCursor(Cursor.HAND);
                itemLabel.setOnMouseClicked(event -> {
                    executeCommand("seber "+itemName);
                });
            }else {
                itemLabel.setTooltip(new Tooltip("Tuto věc nemůžeš vzít!"));
            }
            items.getChildren().add(itemLabel);
        }

    }

    /**
     * Metoda tvořící seznam věcí
     *
     * @return batoh
     */
    private Batoh getSeznamVeci(){
        return hra.getHerniPlan().getBatoh();
    }

    /**
     * Metoda pro položení věcí, zobrazení věcí přidaných do batohu a výměnu mezi hráčem a postavami
     */
    private void updateBacpack() {
        Collection<Vec> backpackList = getSeznamVeci().getVeciZBatohu().values();
        backpack.getChildren().clear();

        for (Vec backpackItem : backpackList) {
            String backpackItemName = backpackItem.getNazev();
            Label backpackItemLabel = new Label(backpackItemName);

            InputStream stream = getClass().getClassLoader().getResourceAsStream(backpackItemName + ".jpg");
            assert stream != null;
            Image img = new Image(stream);
            ImageView imageView = new ImageView(img);
            imageView.setFitWidth(60);
            imageView.setFitHeight(40);
            backpackItemLabel.setGraphic(imageView);

            if (getAktualniProstor().jePostavaVProstoru("obchodník")) {
                backpackItemLabel.setCursor(Cursor.HAND);
                backpackItemLabel.setTooltip(new Tooltip("Koupit si sýr!"));
                backpackItemLabel.setOnMouseClicked(event -> {
                    executeCommand("dej obchodník "+backpackItemName);
                });
            }
            else if (getAktualniProstor().jePostavaVProstoru("poutník")) {
                backpackItemLabel.setCursor(Cursor.HAND);
                backpackItemLabel.setTooltip(new Tooltip("Nakrm poutníka!!"));
                backpackItemLabel.setOnMouseClicked(event -> {
                    executeCommand("dej poutník sýr");
                });
            } else {
                backpackItemLabel.setCursor(Cursor.HAND);
                backpackItemLabel.setOnMouseClicked(event -> {
                    String result = hra.zpracujPrikaz("polož " + backpackItemName);
                    hra.getHerniPlan().getBatoh().odeberVec(backpackItemName);
                    textOutput.appendText(result + "\n\n");

                    update();
                });
            }

            backpack.getChildren().add(backpackItemLabel);

        }
    }

    /**
     * Metoda, která zobrazí postavv ve hře a umožňuje příkaz mluv
     */
    private void updatePersons(){
        Collection<Postava> personList = getAktualniProstor().getPostavy().values();
        persons.getChildren().clear();

        for (Postava person : personList) {
            String personName = person.getJmeno();
            Label personLabel = new Label(personName);

            InputStream stream = getClass().getClassLoader().getResourceAsStream(personName + ".jpg");
            assert stream != null;
            Image img = new Image(stream);
            ImageView imageView = new ImageView(img);
            imageView.setFitWidth(60);
            imageView.setFitHeight(40);
            personLabel.setGraphic(imageView);

            personLabel.setCursor(Cursor.HAND);
            personLabel.setTooltip(new Tooltip("Promluvit si!"));
            personLabel.setOnMouseClicked(event -> {
               executeCommand("mluv " + personName);
                    });



            persons.getChildren().add(personLabel);
        }
    }

    /**
     * Metoda, která provede akci "vykopej hrob"
     */
    private void updateHrob(){
        hrobs.getChildren().clear();
        if((getAktualniProstor().jeVecVProstoru("hrob")));
            String Vykopej = "KOPÁNÍ";
            Label HrobLabel = new Label(Vykopej);
            HrobLabel.setCursor(Cursor.HAND);
            HrobLabel.setOnMouseClicked(event -> {
                executeCommand("vykopej hrob");
            });
           hrobs.getChildren().add(HrobLabel);
    }

    /**
     * Metoda po kliknutí provede akci "odemkni trezor"
     */
    private void updateOdemkni(){
        trezors.getChildren().clear();
        if(getAktualniProstor().jeVecVProstoru("trezor"));
            String Odemkni = "ODEMYKÁNÍ";
            Label TrezorLabel = new Label(Odemkni);
            TrezorLabel.setCursor(Cursor.HAND);
            TrezorLabel.setOnMouseClicked(event -> {
                executeCommand("odemkni trezor");
            });
            trezors.getChildren().add(TrezorLabel);
    }

    /**
     * Metoda sloužící k boji s nepřítelem, provede akci "bojuj trol"
     */
    private void updateBojuj(){
        bojuj.getChildren().clear();
        if(getAktualniProstor().jePostavaVProstoru("trol"));
        String Bojuj = "BOJ";
        Label TrezorLabel = new Label(Bojuj);
        TrezorLabel.setCursor(Cursor.HAND);
        TrezorLabel.setOnMouseClicked(event -> {
            executeCommand("bojuj trol");

        });
        bojuj.getChildren().add(TrezorLabel);
    }

    /**
     * Metoda zobrazující možné východy
     */
    private void updateExits() {
        Collection<Prostor> exitList = getAktualniProstor().getVychody();
        exits.getChildren().clear();

        for (Prostor prostor : exitList) {
            String exitName = prostor.getNazev();
            Label exitLabel = new Label(exitName);

            InputStream stream = getClass().getClassLoader().getResourceAsStream(exitName + ".jpg");
            assert stream != null;
            Image img = new Image(stream);
            ImageView imageView = new ImageView(img);
            imageView.setFitWidth(60);
            imageView.setFitHeight(40);
            exitLabel.setGraphic(imageView);

            if(prostor.isViditelny()) {
                exitLabel.setCursor(Cursor.HAND);
                exitLabel.setTooltip(new Tooltip(prostor.popis));


                exitLabel.setOnMouseClicked(event -> {
                    executeCommand("jdi " + exitName);

                });
            }
            else {
                exitLabel.setTooltip(new Tooltip("Tento prostor zatím není přístupný!"));
            }
            exits.getChildren().add(exitLabel);
        }


    }

    /**
     * Metoda pro zjednosušení příkazů a zejména zpracování příkazů, které se opakují
     *
     * @param command = příkaz
     */
    private void executeCommand(String command) {
        String result = hra.zpracujPrikaz(command);
        textOutput.appendText(result + "\n\n");
        update();
    }

    /**
     * Metoda, která zaznamená stisknutou klávesu a následně zpracuje text, který je před klávesou
     *
     * @param keyEvent = klávesa po, kteríé se zpracuje příkaz
     */
    public void onInputKeyPressed(KeyEvent keyEvent) {

        if(keyEvent.getCode() == KeyCode.ENTER) {
            executeCommand(textInput.getText());
            textInput.setText("");
    }
    }


    public void oHre(ActionEvent actionEvent) {
        Stage stage = new Stage();
        stage.setScene(new Scene(new Label("Tato hra je o...")));
        stage.show();
    }

    /**
     * Metoda, která ukončuje spištěnou hru
     */
    public void konec(){
    Platform.exit();
}
/**
 * Metoda vypíše do textového pole rychlou nápovědu
 */
public void pomoc(){
        String result = hra.zpracujPrikaz("nápověda");
        textOutput.appendText(result + "\n\n");
        update();
}

    /**
     * Metoda, která vytvoří novou hru
     */
    public void novaHra(){
    Stage primaryStage = new Stage();
    primaryStage.setFullScreen(true);
    primaryStage.setTitle("Adventura");

    FXMLLoader loader = new FXMLLoader();
    InputStream stream = getClass().getClassLoader().getResourceAsStream("scene.fxml");

    Parent root = null;
    try {
        root = loader.load(stream);
    } catch (IOException e) {
        e.printStackTrace();
    }
    Scene scene = new Scene(root);
    primaryStage.setScene(scene);
    MainController controller = loader.getController();
    IHra hra = new Hra();
    controller.init(hra);
    primaryStage.show();
}

    /**
     * Metoda, která kontroluje případný konec hry.
     * Když konec nastane, tak znemožní psaní do příkazového řádku a zmrazí hru.
     */
    public void kontrolaKonce(){
        if(hra.konecHry()){
            textInput.setEditable(false);
            hra = null;

        }
}

    /**
     * Metoda zobrazující hráčskou příručku v novém okně
     */
    public void prirucka() {
        Stage secondStage = new Stage();
        WebView webView = new WebView();
        WebEngine webEngine = webView.getEngine();
        webEngine.load( getClass().getResource("/Uživatelská_příručka.html").toString() );

        Scene scene = new Scene(webView,400,400);
        secondStage.setScene(scene);
        secondStage.setTitle("Uživatelská příručka");
        secondStage.show();
    }
}