package cz.vse.kubata.logika;

import cz.vse.kubata.logika.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída SeznamPrikazuTest slouží ke komplexnímu otestování třídy  
 * SeznamPrikazu
 * 
 * @author    Luboš Pavlíček
 * @version   pro školní rok 2016/2017
 */
public class SeznamPrikazuTest
{
    private Hra hra;
    private PrikazKonec prKonec;
    private PrikazJdi prJdi;
    private PrikazSeber prSeber;
    private PrikazProhledniBatoh prProhledniBatoh;
    private PrikazMluv prMluv;
    
    @Before
    public void setUp() {
        hra = new Hra();
        //otestovány pouze určité příkazy, ne všechny
        prKonec = new PrikazKonec(hra);
        prJdi = new PrikazJdi(hra.getHerniPlan(), hra);
        prSeber = new PrikazSeber(hra.getHerniPlan());
        prProhledniBatoh = new PrikazProhledniBatoh(hra.getHerniPlan());
        prMluv = new PrikazMluv(hra.getHerniPlan());
        
    }
  /**
   * Test vložení příkazů do hry.
   */
    @Test
    public void testVlozeniVybrani() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        //otestovány pouze určité příkazy, ne všechny
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prJdi);
        seznPrikazu.vlozPrikaz(prSeber);
        seznPrikazu.vlozPrikaz(prProhledniBatoh);
        seznPrikazu.vlozPrikaz(prMluv);
        assertEquals(prKonec, seznPrikazu.vratPrikaz("konec"));
        assertEquals(prJdi, seznPrikazu.vratPrikaz("jdi"));
        assertEquals(prSeber, seznPrikazu.vratPrikaz("seber"));
        assertEquals(prProhledniBatoh, seznPrikazu.vratPrikaz("prohlédni_batoh"));
        assertEquals(prMluv, seznPrikazu.vratPrikaz("mluv"));
        assertEquals(null, seznPrikazu.vratPrikaz("nápověda"));
    }
    
    /**
     * Test platnosti jednotlivých příkazů.
     */
    @Test
    public void testJePlatnyPrikaz() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        //otestovány pouze určité příkazy, ne všechny
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prJdi);
        seznPrikazu.vlozPrikaz(prSeber);
        seznPrikazu.vlozPrikaz(prProhledniBatoh);
        seznPrikazu.vlozPrikaz(prMluv);
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("konec"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("jdi"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("seber"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("prohlédni_batoh"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("mluv"));
        assertEquals(false, seznPrikazu.jePlatnyPrikaz("Mluv"));
        assertEquals(false, seznPrikazu.jePlatnyPrikaz("bojuj"));
        assertEquals(false, seznPrikazu.jePlatnyPrikaz("nápověda"));
        assertEquals(false, seznPrikazu.jePlatnyPrikaz("Konec"));
    }
    
    /**
     * Test nazvů příkazů. Ukazuje jaké příkazy jsou funkční a jaké ne.
     */
    @Test
    public void testNazvyPrikazu() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        //otestovány pouze určité příkazy, ne všechny
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prJdi);
        seznPrikazu.vlozPrikaz(prSeber);
        seznPrikazu.vlozPrikaz(prProhledniBatoh);
        seznPrikazu.vlozPrikaz(prMluv);
        String nazvy = seznPrikazu.vratNazvyPrikazu();
        assertEquals(true, nazvy.contains("konec"));
        assertEquals(true, nazvy.contains("jdi"));
        assertEquals(true, nazvy.contains("seber"));
        assertEquals(true, nazvy.contains("prohlédni_batoh"));
        assertEquals(true, nazvy.contains("mluv"));
        assertEquals(false, nazvy.contains("Mluv"));
        assertEquals(false, nazvy.contains("bojuj"));
        assertEquals(false, nazvy.contains("nápověda"));
        assertEquals(false, nazvy.contains("Konec"));
    }
    
}
