
package cz.vse.kubata.logika;


import cz.vse.kubata.logika.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;



/*******************************************************************************
 * Testovací třída BatohTest slouží ke komplexnímu otestování třídy 
 * Batoh a jejích metod.
 * 
 *
 * @author  Jan Kubata
 * @version 1.00.0000 — 2020-06-21
 */
public class BatohTest
{


    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
    }


    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }



//\TT== TESTS PROPER ===========================================================

    /***************************************************************************
     * Test funkčnosti Batohu.
     */
    @Test
    public void testBatoh()
    {
        Vec vec1 = new Vec("koš",true);
        Vec vec2 = new Vec("auto",false);
        Vec vec3 = new Vec("kolo",true);
        Vec vec4 = new Vec("kolo2",true);
        Vec vec5 = new Vec("kolo3",true);
        Vec vec6 = new Vec("kolo4",true);
        Vec vec7 = new Vec("kolo5",true);
        
        Batoh batoh1 = new Batoh();
        
        assertEquals(true, batoh1.isMisto());
        assertEquals(true, batoh1.vlozVec(vec1));
        assertEquals(true, batoh1.vlozVec(vec3));
        assertEquals(true, batoh1.vlozVec(vec4));
        assertEquals(true, batoh1.vlozVec(vec5));
        assertEquals(true, batoh1.vlozVec(vec6));
        assertEquals(false, batoh1.isMisto());
        assertEquals(false, batoh1.vlozVec(vec7));
        assertEquals(true, batoh1.obsahujeVec("koš"));
        
        assertEquals(true, batoh1.odeberVec("koš"));
        assertEquals(true, batoh1.isMisto());
        assertEquals(false, batoh1.obsahujeVec("koš"));
        
        assertEquals(false, batoh1.vlozVec(vec2));
    }

}
