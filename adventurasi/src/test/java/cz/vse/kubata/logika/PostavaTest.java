/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package cz.vse.kubata.logika;



import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import cz.vse.kubata.logika.*;


import static org.junit.Assert.*;



/*******************************************************************************
 * Testovací třída PostavaTest slouží ke komplexnímu otestování třídy Postava.
 * 
 *
 * @author  Jan Kubata
 * @version 1.00.0000 — 2020-06-18
 */
public class PostavaTest
{

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
    }


    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }



//\TT== TESTS PROPER ===========================================================

    /***************************************************************************
     * Test funkčnosti třídy Postava.
     */
    @Test
    public void testPostava()
    {
        Prostor prostor1 = new Prostor("náměstí","zkouška");
        Postava postava1 = new Postava("Karel","řeč",true);
        assertEquals(false, prostor1.jePostavaVProstoru("Pepa"));
        assertEquals(false, prostor1.jePostavaVProstoru("Karel"));
        prostor1.vlozPostavu(postava1);
        assertEquals(true, prostor1.jePostavaVProstoru("Karel"));
        assertNotNull(prostor1.vratPostavu("Karel"));
        
    }
/**
 * Test funkčnosti výměny věcí mezi postavami.
 */
    @Test
    public void testVymenyMeziPostavami()
    {
        Postava postava1 = new Postava("Karel","Zdravím, chci pivo!",true);
        Vec vecPivo = new Vec("pivo",true);
        Vec vecVoda = new Vec("voda",true);
        
        
        postava1.nastavVymena(vecPivo, vecVoda, "To nechci!", "Díky, tady máš vodu.", "Čau");
        assertEquals("Zdravím, chci pivo!", postava1.getMluva());
        assertNotNull(postava1.vymena(vecVoda));
        assertEquals("Zdravím, chci pivo!", postava1.getMluva());
        assertNotNull(postava1.vymena(vecPivo));
        assertEquals("Čau", postava1.getMluva());
    }
}
