package cz.vse.kubata.logika;

import cz.vse.kubata.logika.Hra;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída HraTest slouží ke komplexnímu otestování
 * třídy Hra.
 *
 * @author    Jarmila Pavlíčková
 * @version  pro školní rok 2016/2017
 */
public class HraTest {
    private Hra hra1;

    //== Datové atributy (statické i instancí)======================================

    //== Konstruktory a tovární metody =============================================
    //-- Testovací třída vystačí s prázdným implicitním konstruktorem ----------

    //== Příprava a úklid přípravku ================================================

    /***************************************************************************
     * Metoda se provede před spuštěním každé testovací metody. Používá se
     * k vytvoření tzv. přípravku (fixture), což jsou datové atributy (objekty),
     * s nimiž budou testovací metody pracovat.
     */
    @Before
    public void setUp() {
        hra1 = new Hra();
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každé testovací metody.
     */
    @After
    public void tearDown() {
    }

    //== Soukromé metody používané v testovacích metodách ==========================

    //== Vlastní testovací metody ==================================================

    /***************************************************************************
     * Testuje průběh hry, po zavolání každěho příkazu testuje, zda hra končí
     * a v jaké aktuální místnosti se hráč nachází.
     * Při dalším rozšiřování hry doporučujeme testovat i jaké věci nebo osoby
     * jsou v místnosti a jaké věci jsou v batohu hráče.
     * 
     */
    @Test
    public void testPrubehHry() {
        assertEquals("Zapáchající_ulička", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("jdi Netopýří_náměstí");
        assertEquals(false, hra1.konecHry());
        assertEquals("Netopýří_náměstí", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("jdi Krokodýlí_most");
        assertEquals(false, hra1.konecHry());
        assertEquals("Krokodýlí_most", hra1.getHerniPlan().getAktualniProstor().getNazev());
        assertTrue(hra1.getHerniPlan().getAktualniProstor().jeVecVProstoru("peníze"));
        hra1.zpracujPrikaz("seber peníze");
        assertFalse(hra1.getHerniPlan().getAktualniProstor().jeVecVProstoru("peníze"));
        assertTrue(hra1.getHerniPlan().getBatoh().obsahujeVec("peníze"));
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Netopýří_náměstí");
        assertEquals(false, hra1.konecHry());
        assertTrue(hra1.getHerniPlan().getAktualniProstor().jeVecVProstoru("kašna"));
        hra1.zpracujPrikaz("seber kašna");
        assertTrue(hra1.getHerniPlan().getAktualniProstor().jeVecVProstoru("kašna"));
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Obchod_U_Dvou_ptakopysků");
        assertEquals(false, hra1.konecHry());
        assertEquals("Obchod_U_Dvou_ptakopysků", hra1.getHerniPlan().getAktualniProstor().getNazev());
        assertTrue(hra1.getHerniPlan().getAktualniProstor().jePostavaVProstoru("obchodník"));
        hra1.zpracujPrikaz("mluv obchodník");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("dej obchodník peníze");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Netopýří_náměstí");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Zřícenina_Halmar");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("mluv poutník");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("dej poutník sýr");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Hřbitov");
        assertEquals("Hřbitov", hra1.getHerniPlan().getAktualniProstor().getNazev());
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("vykopej hrob");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("seber dýka");
        assertTrue(hra1.getHerniPlan().getBatoh().obsahujeVec("dýka"));
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Zřícenina_Halmar");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Vstupní_hala");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("seber lano");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("polož lano");
        assertEquals(false, hra1.konecHry());
        assertTrue(hra1.getHerniPlan().getAktualniProstor().jeVecVProstoru("lano"));
        hra1.zpracujPrikaz("jdi Pokoj_smrti");
        assertEquals("Pokoj_smrti", hra1.getHerniPlan().getAktualniProstor().getNazev());
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("mluv trol");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("bojuj trol");
        assertTrue(hra1.getHerniPlan().getAktualniProstor().jeVecVProstoru("klíč"));
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("seber klíč");
        assertFalse(hra1.getHerniPlan().getAktualniProstor().jeVecVProstoru("klíč"));
        assertTrue(hra1.getHerniPlan().getBatoh().obsahujeVec("klíč"));
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Vstupní_hala");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Sklepení");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("odemkni trezor");
        assertEquals(false, hra1.konecHry());
        assertTrue(hra1.getHerniPlan().getAktualniProstor().obsahujeVec("jízdenka"));
        hra1.zpracujPrikaz("seber jízdenka");
        assertFalse(hra1.getHerniPlan().getAktualniProstor().jeVecVProstoru("jízdenka"));
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Vstupní_hala");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Zřícenina_Halmar");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Netopýří_náměstí");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Krokodýlí_most");
        assertEquals("Krokodýlí_most", hra1.getHerniPlan().getAktualniProstor().getNazev());
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Výtah_milosti");
        
        
        
        assertEquals(true, hra1.konecHry());
        
        hra1.zpracujPrikaz("konec");
        assertEquals(true, hra1.konecHry());
    }
  
   /**
    * Testuje úmrtí/konec hry v prostoru Nekonečná propast. Smrt nastane
    * pokud hráč vstoupí do prostoru a nemá u sebe lano.
    */
    @Test
    public void testSmrtVPropasti() {
        assertEquals("Zapáchající_ulička", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("jdi Netopýří_náměstí");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Krokodýlí_most");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Nekonečná_propast");
        assertEquals("Nekonečná_propast", hra1.getHerniPlan().getAktualniProstor().getNazev());
        assertEquals(true, hra1.konecHry());
    }
    
   /**
    * Testuje záchranu lanem v prostoru Nekonečná propast. Smrt nastane
    * pokud hráč vstoupí do prostoru a nemá u sebe lano.
    */
    @Test
    public void testLanoVPropasti() {
        assertEquals("Zapáchající_ulička", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("jdi Netopýří_náměstí");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi zřícenina_Halmar");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi vstupní_hala");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("seber lano");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi zřícenina_Halmar");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Netopýří_náměstí");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Krokodýlí_most");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Nekonečná_propast");
        assertEquals("Nekonečná_propast", hra1.getHerniPlan().getAktualniProstor().getNazev());
        assertEquals(true, hra1.konecHry());
    }
}
