package cz.vse.kubata.logika;


import cz.vse.kubata.logika.Prostor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída ProstorTest slouží ke komplexnímu otestování
 * třídy Prostor
 *
 * @author    Jarmila Pavlíčková
 * @version   pro skolní rok 2016/2017
 */
public class ProstorTest
{
    //== Datové atributy (statické i instancí)======================================

    //== Konstruktory a tovární metody =============================================
    //-- Testovací třída vystačí s prázdným implicitním konstruktorem ----------

    //== Příprava a úklid přípravku ================================================

    /***************************************************************************
     * Metoda se provede před spuštěním každé testovací metody. Používá se
     * k vytvoření tzv. přípravku (fixture), což jsou datové atributy (objekty),
     * s nimiž budou testovací metody pracovat.
     */
    @Before
    public void setUp() {
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každé testovací metody.
     */
    @After
    public void tearDown() {
    }

    //== Soukromé metody používané v testovacích metodách ==========================

    //== Vlastní testovací metody ==================================================

    /**
     * Testuje, zda jsou správně nastaveny průchody mezi prostory hry. Prostory
     * nemusí odpovídat vlastní hře, 
     */
    @Test
    public  void testLzeProjit() {
        //otestovány pouze některé prostory
        Prostor prostor1 = new Prostor("zapáchající_ulička","úzká ulička plná smradu");
        Prostor prostor2 = new Prostor("Netopýří_náměstí","Netopýří náměstí, které je sice rozsáhlé, ale uplně prázdné");
        Prostor prostor3 = new Prostor("obchod_U_Dvou_ptakopysků","smíšené zboží");
        Prostor prostor4 = new Prostor("Krokodýlí_most","most, který byl osudný pro mnoho zbloudilých duší");
        Prostor prostor5 = new Prostor("Výtah_milosti",", kde se nachází výtah, kterým se dostaneš zpět na povrch Země. Avšak potřebuješ jízdenku");
        prostor1.setVychod(prostor2);
        prostor2.setVychod(prostor1);
        prostor2.setVychod(prostor3);
        prostor3.setVychod(prostor2);
        prostor2.setVychod(prostor4);
        prostor4.setVychod(prostor5);
        prostor4.setVychod(prostor2);
        prostor5.setVychod(prostor4);
        assertEquals(prostor2, prostor1.vratSousedniProstor("Netopýří_náměstí"));
        assertEquals(prostor1, prostor2.vratSousedniProstor("zapáchající_ulička"));
        assertEquals(prostor3, prostor2.vratSousedniProstor("obchod_U_Dvou_ptakopysků"));
        assertEquals(prostor4, prostor2.vratSousedniProstor("Krokodýlí_most"));
        assertEquals(prostor4, prostor5.vratSousedniProstor("Krokodýlí_most"));
        assertEquals(prostor5, prostor4.vratSousedniProstor("Výtah_milosti"));
        assertEquals(prostor2, prostor3.vratSousedniProstor("Netopýří_náměstí"));
        assertEquals(prostor2, prostor4.vratSousedniProstor("Netopýří_náměstí"));
        assertEquals(null, prostor1.vratSousedniProstor("pokoj"));
    }

}
